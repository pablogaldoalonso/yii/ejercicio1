<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Ejercicio 1 Consultas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección 1</h1>

       
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 1</h2>

                <p>Listar las edades de los ciclistas (Sin repetidos) </p>

                <p>

<?= Html:: a('DAO', ['site/consulta1dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta1orm'], ['class' => 'btn btn-default']) ?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 2</h2>

                <p>Listar las edades de los ciclistas de Artiach</p>
 <p>
                   
<?= Html:: a('DAO', ['site/consulta2dao'], ['class' => 'btn btn-primary']) ?>
     
<?= Html::a('Active Record', ['site/consulta2orm'], ['class' => 'btn btn-default']) ?>
                   
                </p>
            </div>
                    </div>
                </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 3</h2>

                <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>

<?= Html:: a('DAO', ['site/consulta3dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta3orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
                 </div>
            </div>
        <div class="row">
            <div class="col-lg-4">
                 <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 4</h2>

                <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>

<?= Html:: a('DAO', ['site/consulta4dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta4orm'], ['class' => 'btn btn-default']) ?>
            </div>
                      </div>
                 </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 5</h2>
                
                 <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>

<?= Html:: a('DAO', ['site/consulta5dao'], ['class' => 'btn btn-primary']) ?>
                 
<?= Html::a('Active Record', ['site/consulta5orm'], ['class' => 'btn btn-default']) ?>
                
            </div>
                    </div>
                </div>
            <div class="col-lg-4">
                 <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 6</h2>

                <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>

<?= Html:: a('DAO', ['site/consulta6dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta6orm'], ['class' => 'btn btn-default']) ?>
            </div>
                      </div>
                 </div>
            </div>
        <div class="row">
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 7</h2>

                <p>Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo 
                    denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>

<?= Html:: a('DAO', ['site/consulta7dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta7orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
                 </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 8</h2>

                <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.</p>

<?= Html:: a('DAO', ['site/consulta8dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta8orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
                 </div>
            <div class="col-lg-4">
                 <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 9</h2>

                <p>Listar el nombre de los puertos cuya altura sea mayor de 1500. </p>

<?= Html:: a('DAO', ['site/consulta9dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta9orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
            </div>
             </div>
        <div class="row">
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 10</h2>

                <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>

<?= Html:: a('DAO', ['site/consulta10dao'], ['class' => 'btn btn-primary']) ?>
                
<?= Html::a('Active Record', ['site/consulta10orm'], ['class' => 'btn btn-default']) ?>
            </div>
                     </div>
                 </div>
            <div class="col-lg-4">
                <div class ="thumbnail">
                    <div class ="caption">
                <h2>Consulta 11</h2>

                <p>Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>

                <?= Html:: a('DAO', ['site/consulta11dao'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Active Record', ['site/consulta11orm'], ['class' => 'btn btn-default']) ?>
            </div>
                       </div>
                   </div>
            
        </div>

    </div>
</div>
