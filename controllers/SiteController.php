<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionConsulta1dao(){
    $numero = Yii::$app->db
            ->createCommand('select count(distinct edad) from ciclista')
            ->queryScalar();
    
    $dataProvider = new SqlDataProvider([
        'sql'=>'SELECT DISTINCT edad FROM ciclista',
        'totalCount' => $numero,
        'pagination'=>[
            'pageSize' => 5,
        ]  
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['edad'],
        "titulo"=>"Consulta 1 con DAO",
        "enunciado"=>"Listar las edaades de los ciclistas (sin repetidos)",
        "sql" =>"SELECT DISTINCT edad FROM ciclista",
    ]);
        
    }
    
    public function actionConsulta1orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select("edad")->distinct(),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetiddos) ",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
            
        ]); 
    }
    
    public function actionConsulta2dao(){
    $numero = Yii::$app->db
            ->createCommand('SELECT count(distinct edad) FROM ciclista WHERE nomequipo="artiach"')
            ->queryScalar();
    
    $dataProvider = new SqlDataProvider([
        'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach"',
        'totalCount' => $numero,
        'pagination'=>[
            'pageSize' => 5,
       
        ]
    ]) ;
    
    return $this->render ("resultado", [
        "resultados" =>$dataProvider,
        "campos"=>['edad'],
        "titulo"=>"Consulta 2 con DAO",
        "enunciado"=>"Listar las edades de los ciclistas de Artiach",
        "sql" =>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
    ]);
        
    }
    
     public function actionConsulta2orm(){
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select('edad')->distinct()->where("nomequipo = 'artiach'"),
       'pagination' =>[
           'pageSize' =>5,
       ]        
        ]);
        
        return $this->render("resultado", [
        "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach ",
            "sql"=>"SELECT distinct edad FROM ciclista WHERE nomequipo='artiach'",
            
        ]);
          
    }
    
    
    public function actionConsulta3dao(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT count(distinct edad) FROM ciclista WHERE nomequipo="artiach" or nomequipo = "amore vita"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT distinct edad FROM ciclista WHERE nomequipo="artiach" or nomequipo= "amore vita"',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['edad'],
            "titulo" => "Consulta 3 con DAO",
            "enunciado" => "Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql" => 'SELECT distinct edad FROM ciclista WHERE nomequipo="artiach" or nomequipo= "amore vita"',
        ]);
                }
                
                
        public function actionConsulta3orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Ciclista::find()->select ('edad')->distinct () -> where ("nomequipo = 'artiach' or nomequipo = 'amore vita'"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['edad'],
                "titulo" =>"Consulta 3 de Active Record",
                "enunciado" => "Listar las edades de los ciclistas de Artiach o de Amore Vita",
                "sql" => 'SELECT distinct edad FROM ciclista WHERE nomequipo="artiach" or nomequipo= "amore vita"',
                       
            ]);  
        }
        
        public function actionConsulta4dao(){
            
            $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(dorsal) FROM ciclista WHERE edad<25 OR edad>30')
                ->queryScalar();
            
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT  dorsal FROM ciclista WHERE edad<25 OR edad>30',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
           
           
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['dorsal'],
            "titulo" => "Consulta 4 con DAO",
            "enunciado" => "Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql" => 'SELECT dorsal FROM ciclista WHERE edad<25 OR edad>30 ',
        ]);
                
    
        }
        
        public function actionConsulta4orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Ciclista::find()->select ('dorsal')-> where ("edad < 25 or edad >30"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['dorsal'],
                "titulo" =>"Consulta 4 de Active Record",
                "enunciado" => "Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
                "sql" => 'SELECT dorsal FROM ciclista WHERE edad<25 OR edad>30',
                       
            ]);  
        }
        
        public function actionConsulta5dao(){
            
            $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(dorsal) FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = "banesto"')
                ->queryScalar();
            
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = "banesto"',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
           
           
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['dorsal'],
            "titulo" => "Consulta 5 con DAO",
            "enunciado" => "Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql" => 'SELECT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = "banesto"',
        ]);
                
    
        }
        
         public function actionConsulta5orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Ciclista::find()->select ('dorsal')-> where ("edad BETWEEN 28 AND 32 and nomequipo = 'banesto'"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['dorsal'],
                "titulo" =>"Consulta 5 de Active Record",
                "enunciado" => "Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
                "sql" => 'SELECT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = "banesto"',
                       
            ]);  
        }
        
        public function actionConsulta6dao(){
            
            $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT NOMBRE) FROM ciclista WHERE CHAR_LENGTH (nombre) >8')
                ->queryScalar();
            
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT NOMBRE FROM ciclista WHERE CHAR_LENGTH (nombre) >8',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
           
           
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['NOMBRE'],
            "titulo" => "Consulta 6 con DAO",
            "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql" => 'SELECT DISTINCT NOMBRE FROM ciclista WHERE CHAR_LENGTH (nombre) >8',
        ]);
                
    
        }
        
         public function actionConsulta6orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Ciclista::find()->select ('nombre')->distinct()-> where  ("CHAR_LENGTH (nombre) >8"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['nombre'],
                "titulo" =>"Consulta 6 de Active Record",
                "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
                "sql" => 'SELECT DISTINCT NOMBRE FROM ciclista WHERE CHAR_LENGTH (nombre) >8',
                       
            ]);  
        }
        
          public function actionConsulta7dao(){
            
            $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(dorsal), UPPER(nombre) AS Mayusculas FROM ciclista')
                ->queryScalar();
            
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT dorsal, UPPER(nombre) AS Mayusculas FROM ciclista',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
           
           
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['dorsal','Mayusculas'],
            "titulo" => "Consulta 7 con DAO",
          "enunciado" => "Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql" => 'SELECT dorsal, UPPER(nombre) AS Mayusculas FROM ciclista',
        ]);
                
    
        }
        
         public function actionConsulta7orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Ciclista::find()->select("dorsal, UPPER(nombre) AS Mayusculas"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['dorsal','Mayusculas'],
                "titulo" =>"Consulta 7 de Active Record",
                "enunciado" => "Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
                "sql" => 'SELECT dorsal, UPPER(nombre) AS Mayusculas from ciclista',
                       
            ]);  
        }
        
         public function actionConsulta8dao(){
            
            $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM lleva WHERE código = "MGE"')
                ->queryScalar();
            
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE"',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
           
           
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['dorsal'],
            "titulo" => "Consulta 8 con DAO",
            "enunciado" => "Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql" => 'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE"',
        ]);
                
    
        }
        
         public function actionConsulta8orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Lleva::find()->select('dorsal')->distinct()->where("código = 'MGE'"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['dorsal'],
                "titulo" =>"Consulta 8 de Active Record",
                "enunciado" => "Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
                "sql" => 'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE"',
                       
            ]);  
        }
        
        
        public function actionConsulta9dao(){
            
            $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT nompuerto) FROM puerto WHERE altura > 1500')
                ->queryScalar();
            
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
           
           
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['nompuerto'],
            "titulo" => "Consulta 9 con DAO",
            "enunciado" => "Listar el nombre de los puertos cuya altura sea mayor de 1500.",
            "sql" => 'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500',
        ]);
                
    
        }
        
        public function actionConsulta9orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Puerto::find()->select('nompuerto')->distinct()->where ("altura > 1500"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['nompuerto'],
                "titulo" =>"Consulta 9 de Active Record",
                "enunciado" => "Listar el nombre de los puertos cuya altura sea mayor de 1500.",
                "sql" => 'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500',
                       
            ]);  
        }
        
        public function actionConsulta10dao(){
            
            $numero = Yii::$app->db
                ->createCommand('SELECT Count(DISTINCT dorsal) FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000')
                ->queryScalar();
            
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
           
           
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['dorsal'],
            "titulo" => "Consulta 10 con DAO",
            "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql" => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000',
        ]);
                
    
        }
        
         public function actionConsulta10orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Puerto::find()->select('dorsal')->distinct()->where ("pendiente > 8 OR altura between 1800 and 3000"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['dorsal'],
                "titulo" =>"Consulta 10 de Active Record",
                "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
                "sql" => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000',
                       
            ]);  
        }
        
        public function actionConsulta11dao(){
            
            $numero = Yii::$app->db
                ->createCommand('SELECT Count(DISTINCT dorsal) FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000')
                ->queryScalar();
            
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            'pagination' => [
                'pagesize' => 5,   
            ] 
        ]);
           
           
        
        return $this-> render ("resultado", [
            "resultados" => $dataProvider,
            "campos" =>['dorsal'],
            "titulo" => "Consulta 11 con DAO",
            "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql" => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000',
        ]);
                
    
        }
        
         public function actionConsulta11orm(){
            $dataProvider = new ActiveDataProvider([
               'query' => \app\models\Puerto::find()->select('dorsal')->distinct()->where ("pendiente > 8 AND altura between 1800 and 3000"),
                    'pagination' => [
                        'pagesize' => 5,
                    ]     
            ]);
            
            return $this-> render ("resultado",[
                "resultados" =>$dataProvider,
                "campos" =>['dorsal'],
                "titulo" =>"Consulta 11 de Active Record",
                "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
                "sql" => 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000',
                       
            ]);  
        }
        

    
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
